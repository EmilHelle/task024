﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task24.Models
{
    public static class SupervisorGroup
    {
        private static List<Supervisor> supervisors = new List<Supervisor>{
                new Supervisor{Id = 13, Name = "Gustav", Level = "MKUltraLvl", IsAvailable = true},
                new Supervisor{Id = 43, Name = "Sandra", Level = "LollypopLvl", IsAvailable = false},
                new Supervisor{Id = 323, Name = "Rudolf", Level = "JustWeakLvl", IsAvailable = true},
                new Supervisor{Id = 513, Name = "Steiner", Level = "StrongLvl", IsAvailable = false},
                null,
                null,
            };

        public static List<Supervisor> Supervisors { get => supervisors; }

        public static void AddSupervisor(Supervisor supervisor)
        {
            supervisors.Add(supervisor);
        }

        public static List<Supervisor> getFourSupervisors()
        {
            List<Supervisor> fourSupervisors = new List<Supervisor>();
            foreach(Supervisor supervisor in supervisors)
            {
                if(supervisor != null)
                {
                    fourSupervisors.Add(supervisor);
                }
            }
            return fourSupervisors;
        }

        //null conditional operator example
        public new static List<string> ToString()
        {
            List<string> summary = new List<string>();
            foreach (Supervisor supervisor in supervisors)
            {
                int? id = supervisor?.Id;
                string name = supervisor?.Name;
                string level = supervisor?.Level;
                bool? available = supervisor?.IsAvailable;

                summary.Add( $"(Id: {id} Name: {name} Level: {level} Available?: {available}), ");
            }
            return summary;
        }

    }
}

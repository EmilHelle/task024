﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Task24.Models
{
    public class Supervisor
    {
        [Required(ErrorMessage = "You need to enter a number bro")]
        [RegularExpression("^\\d+$", ErrorMessage = "Please enter a whole valid number bro")]
        public int Id { get; set; } = 111;

        [Required(ErrorMessage = "Please enter a name bro")]
        public string Name { get; set; } = "no name";

        [Required(ErrorMessage = "Available or not bro")]
        public bool? IsAvailable { get; set; } = false;

        [Required(ErrorMessage = "Please enter a competence level bro")]
        public string Level { get; set; } = "lowest level";

        public override string ToString()
        {
            return base.ToString();
        }
    }
}

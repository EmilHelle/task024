﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task24.Models;

namespace Task24.Controllers
{
    public class HomeController : Controller
    {

        public IActionResult Index()
        {
            ViewBag.CurrentTime = DateTime.Now;

            return View("HomePage");
        }


        public IActionResult SupervisorList()
        {
            return View(SupervisorGroup.Supervisors.FindAll(supervisor=> supervisor != null));
        }


        public IActionResult AddSupervisor(Supervisor supervisor)
        {
            return View(supervisor);
        }

        [HttpPost]
        public IActionResult CreateSupervisor(Supervisor supervisor)
        {
            if (ModelState.IsValid)
            {
                SupervisorGroup.Supervisors.Add(supervisor);
                return View("SupervisorList", SupervisorGroup.Supervisors.FindAll(supervisorx => supervisorx != null));
            }
            else
            {
                return View("AddSupervisor");
            }
        }

        [HttpGet]
        public IActionResult EditSupervisor(Supervisor supervisor)
        {
            return View(supervisor);
        }

        public IActionResult SupervisorsWithSList()
        {
            List<Supervisor> fourSupervisors = SupervisorGroup.getFourSupervisors().FindAll(supervisor => supervisor.Name.StartsWith("S"));


            return View(fourSupervisors);
        }
    }
}
